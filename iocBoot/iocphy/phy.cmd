#!../../bin/linux-x86_64/phy

## You may have to change phy to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/phy.dbd"
phy_registerRecordDeviceDriver pdbbase

drvAsynIPPortConfigure("phymotion","172.31.180.65:22222",0,0,1)
#drvAsynIPPortConfigure("phymotion","127.0.0.1:22222",0,0,1)

phytronCreateController ("phyMotionPort", "phymotion", 100, 250, 500)

phytronCreateAxis("phyMotionPort", 1, 1)
phytronCreateAxis("phyMotionPort", 2, 1)
phytronCreateAxis("phyMotionPort", 3, 1)
phytronCreateAxis("phyMotionPort", 4, 1)
phytronCreateAxis("phyMotionPort", 5, 1)
phytronCreateAxis("phyMotionPort", 6, 1)
phytronCreateAxis("phyMotionPort", 7, 1)
phytronCreateAxis("phyMotionPort", 8, 1)
phytronCreateAxis("phyMotionPort", 9, 1)
phytronCreateAxis("phyMotionPort", 10, 1)
phytronCreateAxis("phyMotionPort", 11, 1)
phytronCreateAxis("phyMotionPort", 12, 1)
phytronCreateAxis("phyMotionPort", 13, 1)
phytronCreateAxis("phyMotionPort", 14, 1)
phytronCreateAxis("phyMotionPort", 15, 1)
phytronCreateAxis("phyMotionPort", 16, 1)

## Load record instances
dbLoadRecords("db/tbl_safety.db","BL=PINK,DEV=PHY")

cd "${TOP}/iocBoot/${IOC}"

## Load Motor definitions
dbLoadTemplate "motor.substitutions.phytron"

## load BKP records
dbLoadRecords("phybkp.db","BL=PINK,DEV=PHY")
dbLoadTemplate("phybkp.subs", "BL=PINK,DEV=PHY")

## Autosave Path and Restore settings
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

## Autosave Monitor settings
create_monitor_set("auto_settings.req", 30, "P=PINK:PHY,BL=PINK,DEV=PHY")

## Start any sequence programs
#seq sncxxx,"user=epics"

epicsEnvSet("BL", "PINK")
epicsEnvSet("DEV", "PHY")

epicsThreadSleep(5.0)

dbpf("$(BL):$(DEV):AxisA.PROC", "1")
dbpf("$(BL):$(DEV):AxisB.PROC", "1")
dbpf("$(BL):$(DEV):AxisC.PROC", "1")
dbpf("$(BL):$(DEV):AxisD.PROC", "1")
dbpf("$(BL):$(DEV):AxisE.PROC", "1")
dbpf("$(BL):$(DEV):AxisF.PROC", "1")
dbpf("$(BL):$(DEV):AxisG.PROC", "1")
dbpf("$(BL):$(DEV):AxisH.PROC", "1")
dbpf("$(BL):$(DEV):AxisI.PROC", "1")
dbpf("$(BL):$(DEV):AxisJ.PROC", "1")
dbpf("$(BL):$(DEV):AxisK.PROC", "1")
dbpf("$(BL):$(DEV):AxisL.PROC", "1")
dbpf("$(BL):$(DEV):AxisM.PROC", "1")
dbpf("$(BL):$(DEV):AxisN.PROC", "1")
dbpf("$(BL):$(DEV):AxisO.PROC", "1")
dbpf("$(BL):$(DEV):AxisP.PROC", "1")
